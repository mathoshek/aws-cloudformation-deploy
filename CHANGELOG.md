# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.2

- patch: Internal maintenance: add auto-update package version

## 0.4.1

- patch: Internal maintenance: update pipes toolkit version

## 0.4.0

- minor: Stack events are now displayed in the output

## 0.3.0

- minor: Add support for array variable for CAPABILITIES

## 0.2.1

- patch: Pipe now uses the slim version of the base python docker image to improve performance

## 0.2.0

- minor: Add support for CAPABILITIES parameter

## 0.1.1

- patch: Maintenance changes

## 0.1.0

- minor: Initial release

