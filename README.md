# Bitbucket Pipelines Pipe: AWS CloudFormation deploy

Deploy your configuration as code using [AWS CloudFormation](https://aws.amazon.com/cloudformation/).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: atlassian/aws-cloudformation-deploy:0.4.2
  variables:
    AWS_ACCESS_KEY_ID: '<string>'
    AWS_SECRET_ACCESS_KEY: '<string>'
    AWS_DEFAULT_REGION: '<string>'
    STACK_NAME: '<string>'
    TEMPLATE: '<string>'
    # STACK_PARAMETERS: '<json>' # Optional.
    # CAPABILITIES: '<array>' # Optional.
    # WAIT: '<boolean>' # Optional.
    # WAIT_INTERVAL: '<integer>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

### Basic usage

| Variable                     | Usage                                                |
| ------------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*)           |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (*)       |  AWS secret key. |
| AWS_DEFAULT_REGION (*)          |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_. |
| STACK_NAME (*)                  |  The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. |
| TEMPLATE (*)                    |  Location of the file containing a template body in your repository, or a URL that points to the template hosted in an Amazon S3 bucket. Valid template body formats are: `json` or `yaml`. See **Prerequisites**. |
| STACK_PARAMETERS                |  JSON document containing variables to pass to a pipeline you want to trigger. The value should be a list of object with `ParameterKey`, `ParameterValue` fields for each of your variables. The **Examples** section below contains an example for passing parameters to the stack. |
| CAPABILITIES                    |  Array of [capabilities][capabilities]. Allowed values: [`CAPABILITY_IAM, CAPABILITY_NAMED_IAM, CAPABILITY_AUTO_EXPAND`]. |
| WAIT                            |  Wait for deployment to complete. Default: `false`. If false, it will finish when the cloudformation deploy is triggered and will not wait until the resources are successfully created. |
| WAIT_INTERVAL                   |  Time to wait between polling for deployment to complete (in seconds). Default: `30`. |
| DEBUG                           |  Turn on extra debug information. Default: `false`. |
_(*) = required variable._


## Details

This pipe creates or updates a CloudFormation stack associated with the projects AWS cloud infrastructure.

AWS CloudFormation allows you to create and manage AWS infrastructure deployments predictably and repeatedly. You can use AWS CloudFormation to leverage AWS products, such as Amazon Elastic Compute Cloud, S3, etc. to build highly-reliable, highly scalable, cost-effective applications without creating or configuring the underlying AWS infrastructure.


## Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment of your application using CloudFormation.
* You have configured the AWS CloudFormation stack and environment.
* CloudFormation templates greater than 51200 bytes must be deployed from an AWS S3 bucket by providing AWS S3 URL. For further details please see [AWS CloudFormation limits][AWS CloudFormation limits].

## Examples 

### Basic example:

Deploy a new version of your CloudFormation stack, using a template file located in an Amazon S3 bucket.
    
```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.4.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
```

Deploy a new version of your CloudFormation stack, using a template file located in your repository.
    
```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.4.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'stack_template.json'
```

### Advanced example:

Upload a new version of CloudFormation stack template to the AWS S3 Bucket with [AWS S3 deploy pipe](https://bitbucket.org/atlassian/aws-s3-deploy) and then deploy a new version of your CloudFormation stack, using a template file located in an Amazon S3 bucket.

```yaml
script:
  - pipe: atlassian/aws-s3-deploy:0.2.4
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      S3_BUCKET: 'cfn-deploy-pipe'
      LOCAL_PATH: 'directory-with-cfn-template-big-file.json'
  - pipe: atlassian/aws-cloudformation-deploy:0.4.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template-big-file.json'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
```


Deploy a new version of your CloudFormation stack and wait until the deployment has completed.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.4.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
      WAIT: 'true'
      WAIT_INTERVAL: 60
```

Deploy a new version of your CloudFormation stack with additional parameters for the stack.

```yaml
script:
  - pipe: atlassian/aws-cloudformation-deploy:0.4.2
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      STACK_NAME: 'my-stack-name'
      TEMPLATE: 'https://s3.amazonaws.com/cfn-deploy-pipe/cfn-template.json'
      STACK_PARAMETERS: >
          [{
            "ParameterKey": "KeyName",
            "ParameterValue": "mykey"
          },
          {
            "ParameterKey": "DBUser",
            "ParameterValue": "mydbuser"
          },
          {
            "ParameterKey": "DBPassword",
            "ParameterValue": $DB_PASSWORD
          }]
      CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,aws
[capabilities]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#using-iam-capabilities
[AWS CloudFormation limits]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
